doLatr
=======

Idea is : 
========

1. provide social sign in to twitter and as well local user account. 
i.e the signin page has a "sign in with twitter" or local user sign in. 

    a. once the user signs up. and logs in with local account, there's an option to connect to twitter.

    a.i incase of local sign in and connect, the twitter profile info needs to be merged with local account.

    a.ii incase of "sign in with twitter" we auto signup the user for local account. (based on spring social project so far )

    b. import tweets from twitter user timeline stream into mongodb.

    c. provide advanced search all in one search tool bar. (Twitter already has a advanced search feature but the search now clutters the resultset with other user's tweets as well. This app search would be user-specific and also to provide a hashtag hyperlinked search.)

this helps to seamlessly search any articles you've saved up on twitter based on hashtags or text based search. 

2. ability to add tweet like posts to the app (also be able post these to twitter). these posts on app are saved to mongodb just like tweets imported and can be searchable.

3. make this a REST API.

4. build an android app utilizing the REST API.

these are the basic implementation details of the ground work needed but that in itself can be a utility app. However, the core idea is to build a geo-location tagging to the posts that serve as location reminders and notify the user when he/she is around/with in 5 miles of radius. 


Usecase:
========
A real world example would be, say, I want to buy a product at a certain store, that I make a mental note of, while I am home/somewhere else but the store. the chances of forgetting it are more when I'm actually at the store and only realize to have forgot it after being home. The app would be handy in such cases. 

I search for a nearby location using the app whenever I would like to make a note/a to-do at a specific store ( BestBuy, Target, BJs etc ) and post a 'locminder' (a location based reminder) to our app tagged with the location. The next time I'm nearby one of these locations, a notification pops up on my phone ( a service we write, running on phone optimally checks(not draining the battery) for the location and compares with any matching location tags of the locminders, if there's a match, a notification with the locminder shows up).

A further enhancement is to extend the functionality of the "locminder notification service" on the phone to use APIs provided these days by smart watch/ fitness trackers to send a vibrate/tap/buzz to the fitness device, that way you don't need to be looking at the phone to see the notification. there'll be a gentle tap on your wrist and you can check the phone for it. I have used jawbone's UP24 tracker and was looking for an API they would expose to utilize the bluetooth to send alerts to the device (have to check further on this, but for now we'll focus on the basics)