package com.dolatr.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.dolatr.app.controller.UsernameAlreadyInUseException;
import com.dolatr.app.documents.UserAccount;


/**
 * MongoDB Repository for UserAccount entity.
 */
public interface UserAccountRepository extends MongoRepository<UserAccount, String>{
	
	UserAccount findByUserId(String userId);
    
    Page<UserAccount> findAllOrderByUserId(Pageable pageable);
    
    UserAccount findByUsername(String username);
    
    
    
}
