package com.dolatr.app.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.session.web.http.SessionRepositoryFilter;

import com.dolatr.app.account.AuthenticationTokenProcessingFilter;
import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.account.UserAccountServiceImpl;
import com.dolatr.app.account.UserAdminService;
import com.dolatr.app.account.UserAdminServiceImpl;
import com.dolatr.app.repository.MongoSession;
import com.dolatr.app.repository.MongoSessionRepository;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.repository.UserSocialConnectionRepository;
import com.dolatr.app.system.CounterService;
import com.dolatr.app.system.CounterServiceImpl;

@Configuration
@ComponentScan(basePackages = "com.dolatr.app")
@PropertySource("classpath:application.properties")
class MainAppConfig {
    @Inject
    private Environment environment;
//
//    //Repository beans injected from MongoConfig
    @Inject
    UserAccountRepository accountRepository;
    
    @Inject
    UserSocialConnectionRepository userSocialConnectionRepository;
    
    @Inject
    MongoSessionRepository sessionRepository;
    
    @Inject
    CounterService counterService;
//    
    @Inject MongoTemplate mongoTemplate;

    //Application Service beans
    @Bean
    public UserAccountService accountService(CounterService counterService, UserAccountRepository accountRepository,
                    UserSocialConnectionRepository userSocialConnectionRepository, MongoSessionRepository sessionRepository) {
        UserAccountServiceImpl service = new UserAccountServiceImpl(counterService, accountRepository, userSocialConnectionRepository, sessionRepository
                );
        return (UserAccountService) service;
    }

    @Bean
    public UserAdminService userAdminService(MongoTemplate mongoTemplate) {
        return new UserAdminServiceImpl(accountRepository, counterService(mongoTemplate));
    }

    @Bean
    public CounterService counterService(MongoTemplate mongoTemplate) {
        return new CounterServiceImpl(mongoTemplate);
    }
    
	@Bean
	public AuthenticationTokenProcessingFilter authenticationTokenProcessingFilter(UserAccountService userAccountService)
	{	
		return new AuthenticationTokenProcessingFilter(userAccountService);
		
	}

}
