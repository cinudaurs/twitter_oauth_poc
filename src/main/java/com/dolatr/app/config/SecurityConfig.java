package com.dolatr.app.config;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;

import com.dolatr.app.account.AuthenticationTokenProcessingFilter;
import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.repository.MongoSessionRepository;


@ComponentScan
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	
	@Inject
	private Environment environment;

//	@Inject
//	RememberMeTokenRepository rememberMeTokenRepository;
	
	@Inject
	private UserAccountService userAccountService;
	
	@Autowired
	MongoSessionRepository mongoSessionRepository;

//	@Bean
//	public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
//		RememberMeAuthenticationProvider rememberMeAuthenticationProvider = new RememberMeAuthenticationProvider(
//				environment.getProperty("application.key"));
//		return rememberMeAuthenticationProvider;
//	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.httpBasic().disable();
		
		//HAVE TO CHECK SecurityContextPersistenceFilter : http://docs.spring.io/spring-security/site/docs/3.1.x/reference/springsecurity-single.html
		
//		http.sessionManagement()
//		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.authorizeRequests()
				
				.antMatchers("/**").permitAll()
				//.antMatchers("/api/**").permitAll()
				.antMatchers("/index.html", "/views/home.html", "/views/login.html", "/").permitAll()
//	            .antMatchers("/resources/images/**").permitAll()
//	            .antMatchers("/resources/bower_components/**").permitAll()
	            .antMatchers("/api/register").permitAll()
	            .antMatchers("/api/login").permitAll()
	            //.anyRequest().authenticated()
//                .and()
//	            .formLogin()
//                .loginPage("/public/index.html")
//                .defaultSuccessUrl("/connect")
//                .failureUrl("/login?param.error=bad_credentials")
                .and()
                .addFilterAfter(new AuthenticationTokenProcessingFilter(userAccountService), UsernamePasswordAuthenticationFilter.class)
                .csrf().disable()
				.logout();
				//.deleteCookies("JSESSIONID", "authToken")
				//.logoutSuccessUrl("/").permitAll();
		http.apply(new SpringSocialConfigurer());
		
		
	}
	

	

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers("/scripts/**")
            .antMatchers("/resources/**")
            .antMatchers("/css/**")
            .antMatchers("/index.html")
            .antMatchers("/views/login.html")
            .antMatchers("/views/**") 
            .antMatchers("/**")
            .antMatchers("/connect/**")
            ;
    }

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(userAccountService);
			//.passwordEncoder(new BCryptPasswordEncoder());
		
	}
	
	//Override method authenticationManagerBean in WebSecurityConfigurerAdapter to expose 
		//the AuthenticationManager built using configure(AuthenticationManagerBuilder) as a Spring bean. this is important for auth to work.	
		@Bean
		@Override
		   public AuthenticationManager authenticationManagerBean() throws Exception {
		       return super.authenticationManagerBean();
		   }
		
			
		
	
	
}
