package com.dolatr.app.config;

import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ServletWebArgumentResolverAdapter;

import com.dolatr.app.config.EnableMongoHttpSession;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.dolatr.app.controller")
public class WebConfig extends WebMvcConfigurerAdapter {

//    @Override
//    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
//        PageableArgumentResolver resolver = new PageableArgumentResolver();
//        resolver.setFallbackPageable(new PageRequest(1, 10));
//        argumentResolvers.add(new ServletWebArgumentResolverAdapter(resolver));
//    }
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
	    PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver();
	    resolver.setPageParameterName("page.page");
	    resolver.setSizeParameterName("page.size");
	    resolver.setOneIndexedParameters(true);
	    resolver.setFallbackPageable(new PageRequest(1, 10));
	    argumentResolvers.add(resolver);
	    super.addArgumentResolvers(argumentResolvers);
	}
	

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}
    
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/WEB-INF/messages/messages");
		return messageSource;
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/*").allowedOrigins("http://localhost:8080");
            }
        };
    }
	
	
//	@Bean
//	public ViewResolver viewResolver(SpringTemplateEngine templateEngine) {
//		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//		viewResolver.setTemplateEngine(templateEngine);
//		return viewResolver;
//	}
//	
//	@Bean
//	public SpringTemplateEngine templateEngine(TemplateResolver templateResolver) {
//		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//		templateEngine.setTemplateResolver(templateResolver);
//		templateEngine.addDialect(new StandardDialect());
//		templateEngine.addDialect(new LayoutDialect());
//		return templateEngine;
//	}
//	
//	
//	@Bean
//	public TemplateResolver templateResolver() {
//		TemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//		templateResolver.setPrefix("/views/");
//		templateResolver.setSuffix(".html");
//		Set<String> newHtml5TemplatesModePatterns = new HashSet<String>();
//		newHtml5TemplatesModePatterns.add("HTML5");
//		templateResolver.setHtml5TemplateModePatterns(newHtml5TemplatesModePatterns );
//		return templateResolver;
//	}
   
        
    }

