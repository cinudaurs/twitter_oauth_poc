package com.dolatr.app.config;

import java.net.UnknownHostException;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories(basePackages = "com.dolatr.app.repository")
public class MongoConfig {
    @Inject
    private Environment environment;
    
    @Bean
    public MongoDbFactory mongoDbFactory() throws UnknownHostException{
    	
    	MongoClientURI uri = new MongoClientURI("mongodb://localhost/dolatr");
    	return new SimpleMongoDbFactory(new MongoClient(uri), "dolatr");
    	
    }
    
    @Bean
    public MongoTemplate mongoTemplate() throws UnknownHostException{
    	
    	return new MongoTemplate(mongoDbFactory());
    	
    }
    
}





