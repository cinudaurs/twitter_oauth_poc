/*
 * Copyright (c) 2015, Takuya Murakami.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.dolatr.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.SessionRepositoryFilter;

import com.dolatr.app.account.AuthenticationTokenProcessingFilter;
import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.repository.MongoSessionRepository;
import com.dolatr.app.repository.MongoSession;

import javax.servlet.ServletContext;

/**
 * MongoSession Configuration
 */
@Configuration
public class MongoSessionConfiguration {
    
	/**
	 * We need to ensure that our Servlet Container (i.e. Tomcat) uses our springSessionRepositoryFilter for every request.
	 * so we add :
	 **
	 *  <filter>
     *   	<filter-name>springSessionRepositoryFilter</filter-name>
     *   	<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
     *	</filter>
  
     *  <filter-mapping>
     *    	<filter-name>springSessionRepositoryFilter</filter-name>
     *   	<url-pattern>/*</url-pattern>
     *   </filter-mapping>
     *
	 * 
	 * @param repository
	 * @param servletContext
	 * @return
	 */
	
	@Bean
    public SessionRepositoryFilter<com.dolatr.app.repository.MongoSession> springSessionRepositoryFilter(MongoSessionRepository repository, ServletContext servletContext) {
        SessionRepositoryFilter<MongoSession> filter = new SessionRepositoryFilter<>(repository);
        filter.setHttpSessionStrategy(sessionStrategy());
        filter.setServletContext(servletContext);
        return filter;
    }

    @Bean
    public MongoSessionRepository mongoSessionRepository() {
        return new MongoSessionRepository();
    }
    
    @Bean
    HeaderHttpSessionStrategy sessionStrategy() {
      return new HeaderHttpSessionStrategy();
    }
}
