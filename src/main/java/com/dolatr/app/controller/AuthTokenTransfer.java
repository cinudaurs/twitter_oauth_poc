package com.dolatr.app.controller;

public class AuthTokenTransfer
{

	private final String token;


	public AuthTokenTransfer(String token)
	{
		this.token = token;
	}


	public String getToken()
	{
		return this.token;
	}

}
