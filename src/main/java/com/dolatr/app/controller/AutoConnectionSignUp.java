package com.dolatr.app.controller;

import javax.inject.Inject;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionSignUp;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.account.UserAdminService;
import com.dolatr.app.documents.UserAccount;

/**
 * Automatically sign up user who is already signin through other social network account (google or twitter).
 * Create a new UserAccount in database, populate user's profile data from provider.
 */
public class AutoConnectionSignUp implements ConnectionSignUp{
    
	private final UserAdminService adminService;
    
        
    @Inject
    public AutoConnectionSignUp(UserAdminService adminService){
        this.adminService = adminService;
    }
    
    public String execute(Connection<?> connection) {
        ConnectionData data = connection.createData();
        
        UserAccount account = this.adminService.createUserAccount(data, connection.fetchUserProfile());
        
        return account.getUserId();
    }
}
