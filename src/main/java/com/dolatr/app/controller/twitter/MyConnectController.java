package com.dolatr.app.controller.twitter;

import javax.inject.Inject;

import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.stereotype.Controller;

@Controller
public class MyConnectController extends ConnectController{
    
    private static final String ACCOUNT_PAGE = "/#/connect/twitter";
    
    @Inject
    public MyConnectController(ConnectionFactoryLocator connectionFactoryLocator,
            ConnectionRepository connectionRepository) {
        super(connectionFactoryLocator, connectionRepository);
    }

    @Override
    public String connectView(String id) {
        super.connectView(id);
        return "redirect:" + ACCOUNT_PAGE;
        }
    
    @Override
    public String connectedView(String providerId) {
        return "redirect:" + ACCOUNT_PAGE;
    }
    
}