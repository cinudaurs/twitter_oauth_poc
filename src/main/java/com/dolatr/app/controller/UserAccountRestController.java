package com.dolatr.app.controller;


import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.resources.UserAccountResource;
import com.dolatr.app.resources.assemblers.UserAccountResourceAssembler;
import com.dolatr.app.resources.assemblers.UserProfileResourceAssembler;

@Controller
public class UserAccountRestController extends AbstractUserRestController {
    
	private final UserAccountRepository userAccountRepository;
    private final UserAccountResourceAssembler userAccountResourceAssembler;
    
    
    private AuthenticationManager authManager;
    
    private final UserProfileResourceAssembler userProfileResourceAssembler;

    @Inject
    public UserAccountRestController(
            UserAccountService userAccountService, 
            UserAccountRepository userAccountRepository,
            UserAccountResourceAssembler userAccountResourceAssembler,
            AuthenticationManager authManager,
            UserProfileResourceAssembler userProfileResourceAssembler
            ) {
        super(userAccountService);
        this.userAccountRepository = userAccountRepository;
        this.userAccountResourceAssembler = userAccountResourceAssembler;
        this.authManager = authManager;
        this.userProfileResourceAssembler = userProfileResourceAssembler;
    }

    /**
     * Get current user info. If not authenticated, return 401.
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/account")
    public HttpEntity<UserAccountResource> getCurrentUserAccount() {
    	    	
        return new ResponseEntity<>(userAccountResourceAssembler.toResource(getCurrentAuthenticatedUser()), HttpStatus.OK);
    }
    
    
    /**
     * Updates current user profile, like name, email, website, imageUrl.
     * 
     * @param updateMap
     * @return
     * @throws ResourceNotFoundException
     */
    @RequestMapping(method = RequestMethod.PATCH, value = "/account/profile")
    public HttpEntity<Void> patchUserProfile(@RequestBody Map<String, String> updateMap) throws ResourceNotFoundException {
        
    	UserAccount currentUser = getCurrentAuthenticatedUser();
        
        String email = updateMap.get("email");
        if (email != null) {
            currentUser.setEmail(email);
        }
        
        String username = updateMap.get("username");
        if (username != null) {
            currentUser.setUsername(username);
        }        
        userAccountRepository.save(currentUser);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
    

}