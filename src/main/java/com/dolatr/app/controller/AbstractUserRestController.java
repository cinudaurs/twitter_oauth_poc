package com.dolatr.app.controller;

import javax.inject.Inject;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.documents.UserAccount;

@RequestMapping( value = "/api", produces = "application/hal+json" )
public class AbstractUserRestController {
    
	UserAccountService userAccountService;
    
    @Inject
    public AbstractUserRestController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    protected UserAccount getCurrentAuthenticatedUser() {
        UserAccount currentUser = this.userAccountService.getCurrentUser();
        if (currentUser == null) {
            throw new AuthenticationCredentialsNotFoundException("User not logged in.");
        }
        return currentUser;
    }

}