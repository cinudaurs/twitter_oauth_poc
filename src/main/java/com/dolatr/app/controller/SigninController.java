package com.dolatr.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dolatr.app.account.UserAccountService;

@Controller
public class SigninController extends AbstractPageController {

	@Autowired
	private UserAccountService userService;
	
	@RequestMapping(value="/signin", method=RequestMethod.GET)
	public void signin() {
	}

	
}

