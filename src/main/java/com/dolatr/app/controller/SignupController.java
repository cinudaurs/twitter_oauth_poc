package com.dolatr.app.controller;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.session.web.http.CookieHttpSessionStrategy;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.account.UserAdminService;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.message.Message;
import com.dolatr.app.message.MessageType;

@Controller
public class SignupController {

    private final ProviderSignInUtils providerSignInUtils;
    
    private final UserAdminService userAdminService;
    
    @Inject
    UserAccountService userAccountService;
    
    @Autowired
    public SignupController(UserAdminService userAdminService, ConnectionFactoryLocator connectionFactoryLocator, UsersConnectionRepository connectionRepository) {
        this.userAdminService = userAdminService;
        this.providerSignInUtils = new ProviderSignInUtils(connectionFactoryLocator, connectionRepository);
    }

    @RequestMapping(value="/signup", method=RequestMethod.GET)
    public SignupForm signupForm(WebRequest request, Model model) {
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        if (connection != null) {
        	
        	UserAccount userAccount = userAccountService.findByUsername(connection.fetchUserProfile().getUsername());
        	
        	if (userAccount != null){
        		
        		String username = userAccount.getUsername();
        		request.setAttribute("message", new Message(MessageType.INFO, "Your " + StringUtils.capitalize(connection.getKey().getProviderId()) + " account is already associated with a doLatraccount. Please Sign In with the username: "+username), WebRequest.SCOPE_REQUEST);
        		request.setAttribute("localUserExists", username, WebRequest.SCOPE_REQUEST);
        		model.addAttribute("userAccount", username);
        		
        		SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(userAccount.getUserId(), null, null));
                    providerSignInUtils.doPostSignUp(userAccount.getUserId(), request);
            	return new SignupForm();
        	}
        	else
        	{
        		request.setAttribute("localUserExists", null, WebRequest.SCOPE_REQUEST);
        		request.setAttribute("message", new Message(MessageType.INFO, "Your " + StringUtils.capitalize(connection.getKey().getProviderId()) + " account is not associated with a doLatraccount. If you're new, please sign up."), WebRequest.SCOPE_REQUEST);
        		return SignupForm.fromProviderUser(connection.fetchUserProfile());
        	
        	}
            
        } else {
            return new SignupForm();
        }
    }

    
    @RequestMapping(value="/signup", method=RequestMethod.POST)
    public String signup(@Validated SignupForm form, BindingResult formBinding, WebRequest request) {
        if (formBinding.hasErrors()) {
            return null;
        }
        UserAccount account = createAccount(form, formBinding);
        if (account != null) {
            SecurityContextHolder.getContext().setAuthentication(
        new UsernamePasswordAuthenticationToken(account.getUserId(), null, null));
            providerSignInUtils.doPostSignUp(account.getUserId(), request);
            return "redirect:/";
        }
        return null;
    }

    
    private UserAccount createAccount(SignupForm form, BindingResult formBinding) {
    	return userAdminService.createUserAccount(form.getUsername(), form.getPassword());
	}
    
    
}