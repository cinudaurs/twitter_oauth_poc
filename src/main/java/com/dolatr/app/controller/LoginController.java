package com.dolatr.app.controller;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dolatr.app.account.UserAccountService;


@Component
@Controller
@RequestMapping(value = "/api/login")
public class LoginController {
	
	 @Inject
	 private UserAccountService userService;
	 
	 @Inject
	 private AuthenticationManager authManager;
	 
	 
//	  @RequestMapping("/token")
//	  @ResponseBody
//	  public Map<String,String> token(HttpSession session) {
//	    return Collections.singletonMap("token", session.getId());
//	  }
	
	 @RequestMapping(method = RequestMethod.POST, produces = "application/json")
	 public HttpEntity<AuthTokenTransfer> authenticate(@RequestBody UnPwTransfer unpwTransfer, HttpServletRequest request)
		{
	    	
	    	String username = unpwTransfer.getUsername();
	    	String password = unpwTransfer.getPassword();
	    	
			UsernamePasswordAuthenticationToken authenticationToken =
					new UsernamePasswordAuthenticationToken(username, password);
			Authentication authentication = this.authManager.authenticate(authenticationToken);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
						
			HttpSession session = request.getSession(true);
			
			session.setAttribute("username", username);
			session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext().getAuthentication());

			/*
			 * Reload user as password of authentication principal will be null after authorization and
			 * password is needed for token generation
			 */
			UserDetails userDetails = this.userService.loadUserByUsername(username);
			
			AuthTokenTransfer token = new AuthTokenTransfer(TokenUtils.createToken(userDetails, session.getId()));

			return new ResponseEntity<>(token, HttpStatus.OK);
		}
	

}
