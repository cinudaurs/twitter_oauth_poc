package com.dolatr.app.resources;

import org.springframework.hateoas.ResourceSupport;

public class IndexResource extends ResourceSupport {

   private final String name;
   private final String description;
   
   public static final String LINK_NAME_PROFILE = "profile";

   public IndexResource(String name, String description) {
      super();
      this.name = name;
      this.description = description;
   }

   public String getDescription() {
      return description;
   }

   public String getName() {
      return name;
   }
   
   private boolean authenticated = false;

   public boolean isAuthenticated() {
       return authenticated;
   }

   public void setAuthenticated(boolean authenticated) {
       this.authenticated = authenticated;
   }
   
   
   
}