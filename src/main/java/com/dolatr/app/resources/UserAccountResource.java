package com.dolatr.app.resources;

import org.springframework.hateoas.Resource;

import com.dolatr.app.documents.UserAccount;



/**
 * @author Yuan Ji
 */
public class UserAccountResource extends Resource<UserAccount> {
    public static final String LINK_NAME_TWIPOSTS = "twiPosts";
    public static final String LINK_NAME_TWIPOST = "twiPost";
    
    public UserAccountResource(UserAccount account) {
        super(account);
    }

}

