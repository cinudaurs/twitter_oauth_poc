package com.dolatr.app.resources.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.inject.Inject;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.mvc.BasicLinkBuilder;
import org.springframework.stereotype.Component;
import com.dolatr.app.account.UserAccountService;
import com.dolatr.app.controller.IndexController;
import com.dolatr.app.controller.ResourceNotFoundException;
import com.dolatr.app.documents.TwiPost;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.resources.IndexResource;


@Component
public class IndexResourceAssembler {

   private final UserAccountService userAccountService;
//   private final PagedResourcesAssembler<TwiPost> assembler;
   
   @Inject
   public IndexResourceAssembler(UserAccountService userAccountService) {
       this.userAccountService = userAccountService;
      // this.assembler = assembler;
       
   }
   
   
   public IndexResource toResource() {
       
	   UserAccount currentUser = this.userAccountService.getCurrentUser();
       IndexResource resource = new IndexResource("hateoas-api", "A dolatr HATEOAS API");
       resource.setAuthenticated(currentUser != null);
       
       try{
           resource.add(linkTo(methodOn(IndexController.class).getPublicWebsiteResource())
                   .withSelfRel());
           
           String baseUri = BasicLinkBuilder.linkToCurrentMapping().toString();
           
//           Link blogsLink = new Link(
//                   new UriTemplate(baseUri + "/api" + "/twiPosts"), WebsiteResource.LINK_NAME_BLOGS);
//           resource.add(assembler.appendPaginationParameterTemplates(blogsLink));
//           
//           Link blogLink = new Link(
//                   new UriTemplate(baseUri + "/api" + "/twiPosts/{twiPostId}"), WebsiteResource.LINK_NAME_BLOG);
//           resource.add(blogLink);
//           
//           resource.add(linkTo(methodOn(WebsiteRestController.class).getCurrentUserAccount())
//                   .withRel(WebsiteResource.LINK_NAME_CURRENT_USER));
           
//           resource.add(linkTo(methodOn(WebsiteRestController.class).getTagCloud())
//                   .withRel(WebsiteResource.LINK_NAME_TAG_CLOUD));

           Link profileLink = new Link(
                   new UriTemplate(baseUri + "/api"+ "/account/profile"), IndexResource.LINK_NAME_PROFILE);
           resource.add(profileLink);
       
       } catch (ResourceNotFoundException ex) {
       }

       return resource;
   }

}