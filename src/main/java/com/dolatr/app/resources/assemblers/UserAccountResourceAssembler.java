package com.dolatr.app.resources.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import javax.inject.Inject;

import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.mvc.BasicLinkBuilder;
import org.springframework.stereotype.Component;

import com.dolatr.app.controller.UserAccountRestController;
import com.dolatr.app.documents.TwiPost;
import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.resources.UserAccountResource;





/**
 * @author Yuan Ji
 */
@Component
public class UserAccountResourceAssembler implements ResourceAssembler<UserAccount, UserAccountResource> {
    
//	private final PagedResourcesAssembler<TwiPost> assembler;
    
//    @Inject
//    public UserAccountResourceAssembler(PagedResourcesAssembler<TwiPost> assembler) {
//        this.assembler = assembler;
//    }
	
//    @Inject
//    public UserAccountResourceAssembler() {
//        
//    }

    @Override
    public UserAccountResource toResource(UserAccount entity) {
        UserAccountResource resource = new UserAccountResource(entity);

        try {
            resource.add(linkTo(methodOn(UserAccountRestController.class).getCurrentUserAccount())
                    .withSelfRel());
            
            String baseUri = BasicLinkBuilder.linkToCurrentMapping().toString();
            Link twiPostsLink = new Link(
                    new UriTemplate(baseUri + "/api" + "/author/twiPosts"), UserAccountResource.LINK_NAME_TWIPOSTS);
         
//            resource.add(assembler.appendPaginationParameterTemplates(twiPostsLink));
           
            
            Link twiPostLink = new Link(
                    new UriTemplate(baseUri + "/api" + "/author/twiPost"), UserAccountResource.LINK_NAME_TWIPOST);
            resource.add(twiPostLink);

        } catch (Exception ex) {
            //do nothing
        }
        
        return resource;

    }

}
