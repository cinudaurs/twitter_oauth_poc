package com.dolatr.app.account;

import javax.inject.Inject;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.security.SocialUserDetails;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;

public class AccountUtils {
	
	
	private UserAccountRepository userAccountRepository;
	
	
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public UserAccount getLoginUserAccount() {
		
		
		/* You can also do with a thread local security contxt impl : 
		 * https://github.com/nihed/Spring-Social-Twitter-Sample/blob/master/src/main/java/org/springframework/social/quickstart/user/SecurityContext.java 
		 * for storing logged in user info.*/
		
			
		if (getAuthentication() != null) {
			
			UserAccount userAccount = userAccountRepository.findByUsername(getAuthentication().getName());
			
			if (userAccount != null)
				return userAccount;
		
			
		}
		
//		if (getAuthentication() != null
//				&& getAuthentication().getPrincipal() instanceof SocialUserDetails) {
//			
			//load corresponding local user if it exists
		
		return null;
	}

	public String getLoginUserId() {
		UserAccount account = getLoginUserAccount();
		return (account == null) ? null : account.getUserId();
	}

	public AccountUtils() {
	}

	public AccountUtils(UserAccountRepository userAccountRepository) {
		this.userAccountRepository = userAccountRepository;
	}
}
