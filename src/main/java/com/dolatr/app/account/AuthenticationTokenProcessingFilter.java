package com.dolatr.app.account;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.SecurityContext;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

public class AuthenticationTokenProcessingFilter extends GenericFilterBean
{
	
	private final UserAccountService userAccountService;
	private String sessionId;
	private String userName;
	
	public AuthenticationTokenProcessingFilter(UserAccountService userAccountService)
	{
		this.userAccountService = userAccountService;
	}


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException
	{
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);
		
		String currentUrl = UrlUtils.buildRequestUrl((HttpServletRequest) request);

		
		if ( httpRequest.getCookies() == null ){
			
			chain.doFilter(request, response);
			return;
			
		} 	
		
		HttpSession session = httpRequest.getSession();
		
		String authToken = this.extractAuthTokenFromRequest(httpRequest);
		
		if (authToken == null){
			
			authToken = this.extractAuthTokenFromCookie(httpRequest);
			
		}
		
		
		String authTokenQuotesRemoved = authToken.replaceAll("^\"|\"$", "");
		
	
		String sessionId = TokenUtils.getSessionIdFromToken(authTokenQuotesRemoved);

		if (sessionId != null) {
			
			String userName =	userAccountService.findUsernameBySessionId(sessionId);
			
			if (userName != null){

			UserDetails userDetails = this.userAccountService.loadUserByUsername(userName);

			if (TokenUtils.validateToken(authTokenQuotesRemoved, userDetails)) {

				UsernamePasswordAuthenticationToken authentication =
						new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
								
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
			
			}
		}

		chain.doFilter(request, response);

	}





	private String extractAuthTokenFromCookie(HttpServletRequest httpRequest) {
		// TODO Auto-generated method stub
		
		Cookie[] cookies = httpRequest.getCookies();
		
		String cookieValue = null;
		String authToken = null;
		
		for(Cookie cookie:cookies){
			
			if(cookie.getName().toString().equals("authToken")){
				
				cookieValue = cookie.getValue().toString();
				
			}
			
		}
		
		if(cookieValue != null){
		
		try {
			authToken = URLDecoder.decode(cookieValue, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
		return authToken;
		
		
	}


	private HttpServletRequest getAsHttpRequest(ServletRequest request)
	{
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}

		return (HttpServletRequest) request;
	}


	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
	{
		/* Get token from header */
		String authToken = httpRequest.getHeader("X-Auth-Token");

		/* If token not found get it from request parameter */
		if (authToken == null) {
			authToken = httpRequest.getParameter("token");
		}

		return authToken;
	}
}