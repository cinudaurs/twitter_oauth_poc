package com.dolatr.app.account;

import org.springframework.social.UserIdSource;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;

import com.dolatr.app.documents.UserAccount;

/**
 * Domain Service interface for user administration. It also extends SocialUserDetailsService,
 * UserDetailsService and UserIdSource.
 */
public interface UserAdminService extends UserIdSource{
    
    /**
     * Creates a new UserAccount with user social network account Connection Data.
     * Default has ROLE_USER
     * 
     */
    UserAccount createUserAccount(ConnectionData data, UserProfile profile);
    /**
     * Gets current logged in user. Reload UserAccount object from userId in SecurityContextHolder. 
     */
    UserAccount getCurrentUser();

	UserAccount createUserAccount(String username, String password);

}
