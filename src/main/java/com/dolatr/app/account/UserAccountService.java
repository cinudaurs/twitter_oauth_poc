package com.dolatr.app.account;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.security.SocialUserDetailsService;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.documents.UserSocialConnection;


/**
 * Application Service interface for user account. Provide functions based on use cases.
 * 
 */
public interface UserAccountService extends UserDetailsService {
    
    /**
     * Gets UserAccount object by user login userId.
     * 
     * @param userId
     * @return null if not found
     */
    UserAccount findByUserId(String userId);
    
    UserAccount findByUsername(String username);

    /**
     * Gets all users in the database.
     * SECURITY: Current logged in user must have ROLE_ADMIN.
     * 
     * @return
     */
    List<UserAccount> getAllUsers();
    
    /**
     * Gets all users in the database page by page.
     * SECURITY: Current logged in user must have ROLE_ADMIN.
     *  
     * @param pageable
     * @return
     */
    Page<UserAccount> getAllUsers(Pageable pageable);

    /**
     * Gets all social connection records for specific user.
     * 
     * @param userId
     * @return
     */
    List<UserSocialConnection> getConnectionsByUserId(String userId);
    
    
    UserAccount register(UserAccount user);
    
    
    UserAccount addRole(String userId, UserRoleType role);

    /**
     * Remove role from user account.
     * 
     * @param userId
     * @param role
     */
    UserAccount removeRole(String userId, UserRoleType role);

    /**
     * Override SocialUserDetailsService.loadUserByUserId(String userId) to 
     * return UserAccount.
     */
    UserAccount loadUserByUserId(String userId) throws UsernameNotFoundException;
    
    
    
    UserAccount getCurrentUser();
    
    String findUsernameBySessionId(String sessionId);
    
    
}
