package com.dolatr.app.account;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.documents.UserSocialConnection;
import com.dolatr.app.repository.MongoSession;
import com.dolatr.app.repository.MongoSessionRepository;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.repository.UserSocialConnectionRepository;
import com.dolatr.app.system.CounterService;



/**
 * Implementation for AccountService.
 * 
 */
public class UserAccountServiceImpl implements UserAccountService {
    final static Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);

    private final UserAccountRepository accountRepository;
    private final UserSocialConnectionRepository userSocialConnectionRepository;
    private boolean enableAuthorities = true;
    private final CounterService counterService;
    
    private MongoSessionRepository sessionRepository;

	private String username;
    
    public static final String USER_ID_PREFIX = "user";
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAccountServiceImpl.class);
    
    
    @Inject
    public UserAccountServiceImpl(CounterService counterService, UserAccountRepository accountRepository, UserSocialConnectionRepository 
            userSocialConnectionRepository, MongoSessionRepository sessionRepository) {
    	this.counterService = counterService;
        this.accountRepository = accountRepository;
        this.userSocialConnectionRepository = userSocialConnectionRepository;
        this.sessionRepository = sessionRepository;
    }
    

    @Override
    public UserAccount findByUserId(String userId) {
        return accountRepository.findByUserId(userId);
    }

    @Override
    public UserAccount findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserAccount> getAllUsers() {
        return accountRepository.findAll();
    }

    
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<UserAccount> getAllUsers(Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    
    @Override
    public List<UserSocialConnection> getConnectionsByUserId(String userId){
        return this.userSocialConnectionRepository.findByUserId(userId);
    }

	
		public boolean userExists(String username) {
	        
			UserAccount userAccount = accountRepository.findByUsername(username);

	        if (userAccount != null) {
	            return true;
	        }

	        return false;
	    }

		
    protected boolean getEnableAuthorities() {
        return enableAuthorities;
    }

    /**
     * Enables loading of authorities (roles) from the authorities table. Defaults to true
     */
    public void setEnableAuthorities(boolean enableAuthorities) {
        this.enableAuthorities = enableAuthorities;
    }


	@Override
	public UserAccount register(UserAccount user) {
		// TODO Auto-generated method stub
		long userIdSequence = this.counterService.getNextUserIdSequence();
		 
		 UserRoleType[] roles;
		 
		 if (userIdSequence == 1l){
			 
			 roles = new UserRoleType[] { UserRoleType.ROLE_USER, UserRoleType.ROLE_AUTHOR, UserRoleType.ROLE_ADMIN };
			 user.setAdmin(true);
			 user.setEnabled(true);
			 user.setTrustedAccount(true);
			 			 
		 }else {
			 
			 roles =  new UserRoleType[] { UserRoleType.ROLE_USER };
			 user.setAdmin(false);
			 user.setEnabled(true);
			 user.setTrustedAccount(true);
			 
		 }
		 
	     user.setRoles(roles);
	     
	     user.setUserId(USER_ID_PREFIX+userIdSequence);
	  
	     LOGGER.info(String.format("A new user is created (userId='%s') with email '%s'.", user.getUserId(),
	                user.getEmail()));
		
		return accountRepository.save(user);
	}


	 @Override
	    public UserAccount addRole(String userId, UserRoleType role) throws UsernameNotFoundException {
	        UserAccount account = loadUserByUserId(userId);
	        Set<UserRoleType> roleSet = new HashSet<UserRoleType>();
	        for (UserRoleType existingRole : account.getRoles()) {
	            roleSet.add(existingRole);
	        }
	        roleSet.add(role);
	        account.setRoles(roleSet.toArray(new UserRoleType[roleSet.size()]));
	        return this.accountRepository.save(account);
	    }


	 @Override
	    public UserAccount removeRole(String userId, UserRoleType role) throws UsernameNotFoundException {
	        UserAccount account = loadUserByUserId(userId);
	        Set<UserRoleType> roleSet = new HashSet<UserRoleType>();
	        for (UserRoleType existingRole : account.getRoles()) {
	            roleSet.add(existingRole);
	        }
	        roleSet.remove(role);
	        account.setRoles(roleSet.toArray(new UserRoleType[roleSet.size()]));
	        return this.accountRepository.save(account);
	    }


	 @Override
	    public UserAccount loadUserByUserId(String userId) throws UsernameNotFoundException {
	        UserAccount account = accountRepository.findByUserId(userId);
	        if (account == null) {
	            throw new UsernameNotFoundException("Cannot find user by userId " + userId);
	        }
	        return account;
	    }


	@Override
	public UserAccount getCurrentUser() {
		// TODO Auto-generated method stub
		
		 UserAccount user = null;
		 UserDetails loggedUserDetails = null;
	        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        if (authentication != null) {
	            Object principal = authentication.getPrincipal();
	            
	            if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
	    			throw new WebApplicationException(401);
	    		}	            
	            
	            if (principal instanceof UserDetails) {
	                loggedUserDetails = ((UserDetails) principal);
	                String username = loggedUserDetails.getUsername();
	                
	             user = accountRepository.findByUsername(username);
	                
	            } else {
	                throw new RuntimeException("Expected class of authentication principal is AuthenticationUserDetails. Given: " + principal.getClass());
	            }
	         }
			return user;
		
		
		
	}


	@Override
	public String findUsernameBySessionId(String sessionId) {
		
		MongoSession mongoSession = sessionRepository.getSession(sessionId);		

		
		if(mongoSession != null){
			username = mongoSession.getAttribute("username");
		}

		if(username != null)
				return username;
		else
			return null;
	}


	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		UserAccount user = accountRepository.findByUsername(username);
		
		if(user == null){
			
			throw new UsernameNotFoundException(String.format("User with the username %s doesn't exist", username));
			
		}
		
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getAuthorities());
		
		return userDetails;
		
	}
		
}


	
