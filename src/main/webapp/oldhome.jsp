<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>

<html>
<head>
    <title>seedtym</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css"> <!-- load bootstrap css -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body 		{ padding-top:80px; }
    </style>
</head>
<body>
<div class="container">

    <div class="jumbotron text-center">
        <h1> welcome to doLatr </h1>
              
        <c:choose>
			<c:when test = "${empty loggedInUser}">
			 	
			 <a href="signin" class="btn btn-default"><span class="fa fa-user"></span>Signin</a>
      		 <a href="signup" class="btn btn-default"><span class="fa fa-user"></span>Signup</a>
			
		</c:when>
		<c:otherwise>
			Hello ${loggedInUser}! &nbsp;
			<a href="logout" class="btn btn-default"><span class="fa fa-user"></span>Logout</a>
		</c:otherwise>
</c:choose>
        
        


    </div>

</div>
</body>
</html>