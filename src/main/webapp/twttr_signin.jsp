<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>dolatr</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
    
    <link rel="stylesheet" href="resources/css/social-buttons.css" type="text/css">
  
    <link rel="stylesheet" href="https://d3oaxc4q5k2d6q.cloudfront.net/m/f10402dfbb39/compressed/css/f2ba462596cc.css" type="text/css" />
	<link rel="stylesheet" href="https://d3oaxc4q5k2d6q.cloudfront.net/m/f10402dfbb39/compressed/css/d34fc4a32bab.css" type="text/css" />
  
    	
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body 		{ padding-top:80px; }
    </style>
</head>
<body>
<div class="container">

    <div class="col-sm-6 col-sm-offset-3">

        <h1><span class="fa fa-sign-in"></span> Login</h1>
        <hr>

        <!-- LOGIN FORM -->
        <form action="login" method="post">
        
            <div class="form-group">
                <label>Username</label>
                <input type="text" id="userId" name="userId" placeholder="user name" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" id="password" name="password" placeholder="password" class="form-control"/>
            </div>

            <button type="submit" class="btn btn-warning btn-lg">Login</button>
        </form>

        </br>
        <p>Need an account? <a href="signup">Signup</a></p>
        
        <h3>Connect to Twitter</h3>

	<form action="<spring:url value='/connect/twitter'/>" method="POST">
  		  <div class="formInfo">
      		  <p>You aren't connected to Twitter yet. Click the button to connect this application with your Twitter account.</p>
   		 </div>
   		 <p><button type="submit">Connect to Twitter</button></p>
	</form>
  	
	</div>
  
</div>





</body>
</html>


