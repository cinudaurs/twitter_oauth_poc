(function () {
    'use strict';

    angular
        .module('app')
        .constant("baseURL", "http://localhost:8080")
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', '$rootScope', 'AuthenticationService', 'MyAccountService','FlashService', 'baseURL'];
    function LoginController($location, $rootScope, AuthenticationService, MyAccountService, FlashService) {
        var vm = this;

        vm.login =  function login() {
        	
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (authenticationResult) {
            	
                if (authenticationResult) {
                	
//                	MyAccountService.load()
//              	   .then(function(resource) {
//           		   $rootScope.user = resource;
//           		   $location.path('/');
//              	   });
                	
                	$location.path('/');
                	console.log("Login succeeded")
                    
                
                } else {
                	
                	console.log("Login failed")
				    FlashService.Error(authenticationResult);
                    vm.dataLoading = false;
                }
            });
        };




    }

})();