angular.module('twiportr.home',
		['twiportr.home.controllers',
		 'twiportr.home.directives',
		 'twiportr.home.services','twiportr.home.filters', 'ui.router', 'ngResource']);

angular.module('twiportr.home').config(['$stateProvider','$locationProvider', '$httpProvider',function($stateProvider,$locationProvider, $httpProvider){
	
	$stateProvider
	.state('register', 
	{
		url:'/register',
		controller:'RegisterController',
		templateUrl: '/twiportr/app/modules/home/views/register.html'		
	})
	.state('login',
	{    url:'/login',    
		 controller:'LoginController',    
		 templateUrl:'/twiportr/app/modules/home/views/login.html'
	})
	.state('home',
	{
		url:'/home',
		controller: 'HomeController',
		templateUrl: '/twiportr/app/modules/home/views/home.html'		
	})
	.state('home.twitter', 
	{
		url:'/connect',
		templateUrl: '/twiportr/app/modules/home/views/twitter.html'
	})
	.state('home.allPosts',
	{
		url:'/twiposts',
		templateUrl: '/twiportr/app/modules/home/views/twiposts.html',
		controller: 'TwipostController'
	});
	
	$httpProvider.interceptors.push('authInterceptor');
	
}]);

angular.module('twiportr.home').factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) 
		{  
			return {    
					// Add authorization token to headers    
				request: function (config) 
				{      
					config.headers = config.headers || {};      
				
				if ($cookieStore.get('authToken')) 
				{        
					
        				config.headers['X-Auth-Token'] = $cookieStore.get('authToken');
        			
				}
				
				return config;
				
				},
				
				// Intercept 401s and redirect you to login    
				responseError: function(response) 
				{      
					if(response.status === 401) 
					{   
						$location.path('/login');        
						// remove any stale tokens        
						$cookieStore.remove('authToken');        
						return $q.reject(response);      
					}      
					else {        
						
							return $q.reject(response);      
						}    
					}  
			};
					
		})						