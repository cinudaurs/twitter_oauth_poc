angular.module('twiportr.home.controllers',[])

.controller('HomeController',
			['$scope', 'AuthenticationService', '$state', function($scope, AuthenticationService, $state){
				
				   $scope.logout=function(){        
					   
					   authService.logout().then(function()
							   {            
						  
						   $state.go('login');        
						
							   });    
					   }
				
				
			}])
			.controller('TwipostController',
		   ['$scope', '$state', 'Twipost', function($scope, $state, Twipost){    

			   
			   $scope.twiPost = new Twipost();
			   
			   $scope.twiPost.text = ''
			   
			   $scope.buttonText = "Jot"
			   
			   
			   
			   $scope.twiposts = Twipost.query();
			  			   
			   
			   $scope.addPost = function() {
				   
				     $scope.twiPost.text = $scope.content
				   
				     $scope.buttonText = "saving..."
				   
				   	 $scope.twiPost.$save(function(){
				   		
				   		$state.go('home.allPosts', {}, {reload: true});   //this was needed to reload- http://stackoverflow.com/a/23609343
				   		
				   		 
				   	 });
				   }
			   			   
			   $scope.cancel = function(){
				   
				   $scope.content = '';
				   
			   }
			   
			   
			   
			   
           }]).controller('LoginController',
        		   ['$scope','AuthenticationService','$state',function($scope,AuthenticationService,$state){    
        			   
        			   $scope.buttonText="Login";   
        			          			  
        			   $scope.login= function login() {
        	
        				   vm.dataLoading = true;
        				   AuthenticationService.Login($scope.credentials.username,$scope.credentials.password, function (authenticationResult) {
            	
        					   if (authenticationResult) {
                	
        						   $state.go('home.allPosts'); 
        						   console.log("Login succeeded")
                    
                
        					   } else {
                	
        						   $scope.invalidLogin=true;        
            					   $scope.errorMsg = 'Invalid username/password';
            					   $scope.buttonText="Login";
                }
            });
        };
        			   
        		   }])
          .controller('RegisterController',
        		   ['$scope','AuthenticationService','$state',function($scope,AuthenticationService,$state){ 
        		       
        			   $scope.buttonText="Register";
        		
        			   $scope.register = function() {
        		        	
        		        	$scope.buttonText="Registering ...";
        		        	AuthenticationService.Register($scope.user)
        		                .then(function (data) {
        		                    if (data) {
        		                                		                        
        		                        $scope.success = true;
        	        					$scope.registerSuccessMsg = 'Registered! Please Login'
        		                        
        		                        $rootScope.username = data.username;
        		                        
        		                        $location.path('/login');
        		                    } else {
        		                    	   $scope.error = true;
        	        					   $scope.errorMsg = 'Registration Failed! Please contact site-admin'
        		                    }
        		                    $scope.buttonText="Register";
        		        			   
        		                });
        		        };
        		        
        		   }]);       			   
        			 