angular.module('twiportr.home.services', [])

.factory('Twipost',
		['$resource','API_ENDPOINT',
		 function($resource,API_ENDPOINT)
		 {    return $resource(API_ENDPOINT, { id: '@id' });
		 }])

.factory('authService',['AUTH_ENDPOINT','REGISTER_ENDPOINT', 'LOGOUT_ENDPOINT','$http','$cookieStore', '$q',function(AUTH_ENDPOINT,REGISTER_ENDPOINT, LOGOUT_ENDPOINT,$http,$cookieStore, $q){

			    var auth={};

			    auth.register=function(user){
			    	
			    	return $http.post(REGISTER_ENDPOINT, user).then(handleSuccess, handleError('Error creating user'));
			    	
			    	
			    }
			    
			    function handleSuccess(data) {
		            return data;
		        }

		        function handleError(error) {
		            return function () {
		                return { success: false, message: error };
		            };
		        }
			    			    
			    
			    auth.login=function(username,password, callback){
			    	
			    	 var cb = callback || angular.noop;
			    	 
			    	 var deferred = $q.defer();
			    	 
			    	 $http.post(AUTH_ENDPOINT,{username:username,password:password})
			    	 .success(function(response){
			            authToken = response.data;
			            $cookieStore.put('authToken', authToken);
			            deferred.resolve(response);
			            return cb();
			            
			        }).error(function(err){
			        	
			        	auth.logout();
			        	deferred.reject(err);
			        	return cb(err);			        	
			        	
			        }.bind(this));
			    	 
			    	 return deferred.promise;
			    	 
			    }
			    
			    auth.logout=function(){
			        return $http.post(LOGOUT_ENDPOINT).then(function(response){
			            
			        	authToken = undefined;
			            $cookieStore.remove('authToken');
			        
			        });
			    }

			    return auth;

			}]);
		 
		 
angular.module('twiportr.home.services').value('API_ENDPOINT','http://localhost:8080/api/twiposts');
angular.module('twiportr.home.services').value('AUTH_ENDPOINT','http://localhost:8080/api/login');
angular.module('twiportr.home.services').value('LOGOUT_ENDPOINT','http://localhost:8080/api/logout'); 
angular.module('twiportr.home.services').value('REGISTER_ENDPOINT','http://localhost:8080/api/register'); 
