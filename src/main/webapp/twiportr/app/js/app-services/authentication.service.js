(function () {
    'use strict';

    angular
        .module('app')
        .constant("baseURL", "http://localhost:8080")
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$q', '$timeout', 'UserService', 'MyAccountService', '$location', 'baseURL'];
    function AuthenticationService($http, $cookieStore, $rootScope, $q, $timeout, UserService, MyAccountService, $location, baseURL) {
        
    	var service = {};

        
        service.Login = Login;
        service.SetAuthToken = SetAuthToken;
        service.ClearAuthToken = ClearAuthToken;

        return service;
        
        function Login(username, password, callback) {
    		
        	$http.post(baseURL + '/api/login', {'username':username, 'password':password}).success(function(authenticationResult) {
				
        		
        		console.log(authenticationResult.token)
        		
				if (authenticationResult) {
					
                	var authToken = authenticationResult.token;
                	$rootScope.authToken = authToken;
					$rootScope.authenticated = true;
					
					
					SetAuthToken(authToken);
					
					
				} else {
					
					$rootScope.authenticated = false;
				
				}
				callback && callback($rootScope.authenticated);
			}).error(function() {
				$rootScope.authenticated = false;
				callback && callback(false);
			});

		}

        function SetAuthToken(authToken) {
        	$cookieStore.put('authToken', authToken);
        }

        function ClearAuthToken() {
            $rootScope.authToken = {};
            $cookieStore.remove('authToken');            
        }
    }

})();