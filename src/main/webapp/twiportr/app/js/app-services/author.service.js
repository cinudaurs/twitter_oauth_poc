angular.module('app').factory('AuthorService', [ 'halClient', function(halClient) {
    
    return {
        'load' : 
            function() {
                return halClient.$get('/api/author');
            }
    };
    
}]);