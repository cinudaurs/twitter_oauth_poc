(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies', 'angular-hal', 'ui.bootstrap'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];
    function config($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
        	.when('/', {
        		controller: 'HomeController',
        		templateUrl: 'views/home.html',
        		controllerAs: 'vm'
        	})
        	 .when('/login', {
                controller: 'LoginController',
                templateUrl: 'views/login.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'views/register.html',
                controllerAs: 'vm'
            }).
            when('/myaccount', {
            	controller : 'MyAccountController',
            	templateUrl : 'views/account.html'
            })
//            .
//            when('/twiposts', {
//            	controller : 'MyTwiPostListController',
//            	templateUrl : 'twipost/mytwiposts-list.html'
//            	})
            .otherwise({ redirectTo: '/login' });
        
        
        var interceptor = ['$q', '$location', function ($q, $location) {

            function success(response) {
                return response;
            }

            function error(response) {
                if (response.status === 404) {
                    $location.path("/404").replace();
                } else if (response.status === 403) {
                    $location.path("/403").replace();
                } else if (response.status === 401) {
                    $location.path("/401").replace();
                }
                
                return $q.reject(response);
            }

            return function (promise) {
                return promise.then(success, error);
            };
        }];
        
        
        /* Register error provider that shows message on failed requests or redirects to login page on
    	 * unauthenticated requests */
        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
    	        return {
    	        	'responseError': function(rejection) {
    	        		var status = rejection.status;
    	        		var config = rejection.config;
    	        		
    	        		var url = config.url;
    	      
    	        		if (status == 401) {
    	        			$location.path( "/login" );
    	        		} else {
    	        			$rootScope.error = " on " + url + " failed with status " + status;
    	        		}
    	              
    	        		return $q.reject(rejection);
    	        	}
    	        };
    	    }
        );  
        
        
        /* Registers auth token interceptor, auth token is either passed by header or by query parameter
         * as soon as there is an authenticated user */
        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
            return {
            	'request': function(config) {
            		
            		if (angular.isDefined($rootScope.authToken)) {
            			var authToken = $rootScope.authToken;
            			if (appConfig.useAuthTokenHeader) {
            				config.headers['X-Auth-Token'] = authToken;
            			} else {
            				config.url = config.url + "?token=" + authToken;
            			}
            		}
            		return config || $q.when(config);
            	}
            };
        }
    );
        
        
    }
   
    

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
                
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/myaccount', '/twiposts']) === -1;
            
            var originalPath = $location.path();
            
            var authToken = $cookieStore.get('authToken');
            
            if (authToken !== undefined) {
    			$rootScope.authToken = authToken;
    			$location.path(originalPath);
    		}
            
            
            if (restrictedPage && !authToken) {
                $location.path('/login');
            }
        });
    }

})();