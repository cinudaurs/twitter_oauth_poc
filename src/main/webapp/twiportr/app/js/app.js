'use strict'

//angular.module('twiportr').run(['$state','$rootScope',function($state,$rootScope){
//
//    $state.go('login');
//}]);


angular.module('twiportr',
		['ngCookies','twiportr.controllers',
		 'twiportr.directives',
		 'twiportr.services','twiportr.filters', 'ui.router', 'ngResource'])

.config(config)
.run(run);

config.$inject = ['$stateProvider', '$locationProvider', '$httpProvider', '$urlRouterProvider'];
function config($stateProvider, $locationProvider, $httpProvider, $urlRouterProvider) {
	
	$stateProvider
	.state('register', 
	{
		url:'/register',
		controller:'RegisterController',
		templateUrl: '/twiportr/app/views/register.html'		
	})
	.state('login',
	{    url:'/login',    
		 controller:'LoginController',    
		 templateUrl:'/twiportr/app/views/login.html'
	})
	.state('home',
	{
		url:'/home',
		controller: 'HomeController',
		templateUrl: '/twiportr/app/views/home.html'		
	})
	.state('home.twitter', 
	{
		templateUrl: '/twiportr/app/views/twitter.html',
	//	controller: 'TwitterConnectController'
	})
	.state('home.allPosts',
	{
		url:'/twiposts',
		templateUrl: '/twiportr/app/views/twiposts.html',
		controller: 'TwipostController'
	})
	.state('twitterRedirectAfterAuth',
	{	
		url: '/connect/twitter',
		controller : 'TwitterConnectController'
		
	})
	.state('home.twitterConnected',
	{	
		url: '/connected/twitter',
		templateUrl: '/twiportr/app/views/twitterConnected.html',
		controller : 'TwitterAfterConnectController'		
		
	})
	.state('home.importTweets', 
		{	
			url: '/connected/twitter',
			templateUrl: '/twiportr/app/views/twitterConnected.html',
			controller : 'TwitterAfterConnectController'			
		}
	
	);
	
	$urlRouterProvider.otherwise('/home');
	
	var appConfig = {
			/* When set to false a query parameter is used to pass on the auth token.
			 * This might be desirable if headers don't work correctly in some
			 * environments and is still secure when using https. */
			useAuthTokenHeader: true 	
		};	
	
	
	$httpProvider.defaults.useXDomain = true;
	$httpProvider.defaults.withCredentials = true;
	delete $httpProvider.defaults.headers.common["X-Requested-With"];
	$httpProvider.defaults.headers.common["Accept"] = "application/json";
	$httpProvider.defaults.headers.common["Content-Type"] = "application/json";
	
	
		
	var interceptor = ['$q', '$location', function ($q, $location) {

        function success(response) {
            return response;
        }

        function error(response) {
            if (response.status === 404) {
                $location.path("/404").replace();
            } else if (response.status === 403) {
                $location.path("/403").replace();
            } else if (response.status === 401) {
                $location.path("/401").replace();
            }
            
            return $q.reject(response);
        }

        return function (promise) {
            return promise.then(success, error);
        };
    }];
    
    
    /* Register error provider that shows message on failed requests or redirects to login page on
	 * unauthenticated requests */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
	        return {
	        	'responseError': function(rejection) {
	        		var status = rejection.status;
	        		var config = rejection.config;
	        		
	        		var url = config.url;
	      
	        		if (status == 401) {
	        			$location.path( "/login" );
	        		} else {
	        			$rootScope.error = " on " + url + " failed with status " + status;
	        		}
	              
	        		return $q.reject(rejection);
	        	}
	        };
	    }
    );  
    
    
    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
     * as soon as there is an authenticated user */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location, $cookieStore) {
        return {
        	'request': function(config) {
        		
        		if (angular.isDefined($rootScope.authToken)) {
        			
        			var authToken = $rootScope.authToken;
        			
        			if (appConfig.useAuthTokenHeader) {
        				config.headers['X-Auth-Token'] = authToken;
        			} else {
        				config.url = config.url + "?token=" + authToken;
        			}
        		}
        		else{
        			
        			var authToken = $cookieStore.get('authToken');
        	        
        	        console.log(authToken);
        	        
        	        if (appConfig.useAuthTokenHeader) {
        				config.headers['X-Auth-Token'] = authToken;
        			} else {
        				config.url = config.url + "?token=" + authToken;
        			}
        			
        			
        			
        			
        		}
        		
        		
        		return config || $q.when(config);
        	}
        };
    }
);
}

run.$inject = ['$rootScope', '$location', '$cookieStore', '$http', '$state'];
function run($rootScope, $location, $cookieStore, $http, $state) {
	        
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
//        // redirect to login page if not logged in and trying to access a restricted page
//        var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/home', '/twiposts']) === -1;
//        
        var originalPath = $location.path();
        
        var authToken = $cookieStore.get('authToken');
        
        console.log(authToken);
        
        if (authToken !== undefined) {
			$rootScope.authToken = authToken;
			
			console.log($rootScope.authToken);
			
			$location.path(originalPath);
		}
        
        
        if (!authToken) {
            $location.path('/login');
        }
        
        
        
    });
}
;
	
