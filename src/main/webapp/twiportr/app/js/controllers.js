'use strict'

angular.module('twiportr.controllers',[])

.controller('HomeController',
			['$scope', 'authService', '$state', function($scope, authService, $state){
				
				   $scope.logout=function(){        
					   
					   authService.logout().then(function()
							   {            
						  
						   $state.go('login');        
						
							   });    
					   }
				
				
			}])
			.controller('TwipostController',
		   ['$scope', '$state', 'Twipost', function($scope, $state, Twipost){    

			   
			   $scope.twiPost = new Twipost();
			   
			   $scope.twiPost.text = ''
			   
			   $scope.buttonText = "Jot"
			   
			   
			   
			   $scope.twiposts = Twipost.query();
			  			   
			   
			   $scope.addPost = function() {
				   
				     $scope.twiPost.text = $scope.content
				   
				     $scope.buttonText = "saving..."
				   
				   	 $scope.twiPost.$save(function(){
				   		
				   		$state.go('home.allPosts', {}, {reload: true});   //this was needed to reload- http://stackoverflow.com/a/23609343
				   		
				   		 
				   	 });
				   }
			   			   
			   $scope.cancel = function(){
				   
				   $scope.content = '';
				   
			   }
			   
           }])
           
           
           .controller('TwitterAfterConnectController', ['$rootScope', '$scope', '$state', 'twitterService', function($rootScope, $scope, $state, twitterService){ 
        	
        	   
        	   if($rootScope.ImportButtonActive == undefined){
        		
        		   $rootScope.ImportButtonActive = true;
            	   $rootScope.buttonText="Import Tweets";        		   
        	   }
        	      
        	   	
        	   $scope.importTweets = function(){
        		   
        		   $rootScope.buttonText="Importing...";
        		   $rootScope.ImportButtonActive = false;
        		   $state.go('home.twitterConnected');
        		   
        		   twitterService.importTweets(function(data){
        			   
        			   if(data){
        				
        				   $state.go('home.allPosts');
            			   console.log("import in progress");
        				   
        			   }
        			   
        			   
        			   
        		   });
        		   
        	   }
           
           }])
           
           
           
            //this doesn't do anything. working with ajax calls to twitter's api.twitter was resulting in CORS as requested resource
            // doesn't have allow-origin header. twitter doesn't support it. all it needed was a html form with action='/connect/twitter'
           //and method post.
           .controller('TwitterConnectController',
		   ['$rootScope', '$state', 'twitterService', function($rootScope, $state, twitterService){    

			   $rootScope.connectedTwitter = true;
			   $state.go('home.twitterConnected');
			   
           }])
           
           
           
           
           
           
           .controller('LoginController',
        		   ['$scope','authService','$state',function($scope,authService,$state){    
        			   
        			   $scope.buttonText="Login";
        			   
        			   $scope.invalidLogin=false;
        			           			  
        			   $scope.login=function(){     
        				   
        				   $scope.buttonText="Logging in. . .";    
        				   
        				         				   
        				   
        				   
        				   
        				   
        				           				   
        				   authService.Login($scope.credentials.username,$scope.credentials.password,function (authenticationResult) {

        					   if (authenticationResult) {
        					
        					   $state.go('home.allPosts');   
        					   console.log("Login succeeded");
        					   }else{
        						   
        						   $scope.invalidLogin=true;        
            					   $scope.errorMsg = 'Invalid username/password';
            					   $scope.buttonText="Login";
        						   
        					   }
        					   
        				   
        			   });
        			   
        			   };
        			   
        			   
        		   }])
          .controller('RegisterController',
        		   ['$scope','authService','$state',function($scope,authService,$state){ 
        		       
        			   $scope.buttonText="Register";
        			   
        			   $scope.register=function(){
        				
        				   $scope.buttonText="Registering ...";
        				           				   
        				   authService.Register($scope.user).then(function(data){
        					   
        					   $scope.success = true;
        					   $scope.registerSuccessMsg = 'Registered! Please Login'
        						   
        					   //$state.go('login');
        					   
        				   }, function(err){
        					   
        					   $scope.error = true;
        					   $scope.errorMsg = 'Registration Failed! Please contact site-admin'
        					         					   
        				   })
        			   }
        			   $scope.buttonText="Register";
        			   
        		   }]);


